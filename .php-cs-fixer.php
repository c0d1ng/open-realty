<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('include/language')
    ->exclude('install/language')
    ->exclude('tests')
    ->exclude('vendor')
    ->exclude('node_modules')
    ->exclude('template')
    ->exclude('addons')
    ->in('src')
;

$config = new PhpCsFixer\Config();
return $config->setRules([
        '@PSR2' => true,
        //'@PHP74Migration' => true,
        //'strict_param' => true,
        //'array_syntax' => ['syntax' => 'short'],
    ])
    ->setFinder($finder)
;
