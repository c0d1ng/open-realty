<?php
error_reporting(E_ALL ^ E_NOTICE);
// just so we know it is broken
session_start([
    "cookie_httponly" => true,
    "cookie_samesite" => "Strict",
    "use_strict_mode" => true,
    "sid_bits_per_character" => 6
]);
require_once dirname(__FILE__).'/base_installer.php';
$installer = new installer();
$installer->show_header();
$installer->run_installer();
$installer->show_footer();
