#!/usr/bin/env bash

#
# This is a script that should be run before each commit, so called Git Hook (see: http://githooks.com/)
#
# This file will be linked in .git/hooks/pre-commit by the recipe in the Makefile and therefore run before each commit.
#
# What does it exactly do?
# The main objective of this file is to run `make review` command on the state of the code in your git index (staging).
#
# To do this it will try to:
#  - Create a temporary directory. This is necessary, because the working copy may contain changes that you are
#    currently working on and do not want to commit yet. Therefore the changes should not affect the outcome of the
#    review. Using a temporary directory instead of git stash makes sure that there will be no issue with eventual conflicts.
#    See: http://stackoverflow.com/questions/20479794/how-do-i-properly-git-stash-pop-in-pre-commit-hooks-to-get-a-clean-working-tree
#  - Checkout the state of the index of your git repository (i.e. what you are trying to commit) to the temporary directory.
#  - Run `make review` command.
#  - Remove the temporary directory.
#
# To learn more about bash scripting, please read: http://s.ntnu.no/bashguide.pdf
#
#                                                   !!!DEBUGGING!!!
#
#       In order to debug this script you will probably want to comment out the line removing temporary directory.
#                                           (See removeTmpReviewDir function)
#
#                                           Then inspect the process manually.
#
#                                                   !!!DEBUGGING!!!
#

# fix for SourceTree, on MacOS terminals and full .apps have completely different environments (and PATH variables)
# see: https://community.atlassian.com/t5/Bitbucket-questions/SourceTree-Hook-failing-because-paths-don-t-seem-to-be-set/qaq-p/274792
source ~/.bash_profile

# the directory of the script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# temporary review directory
TMP_REVIEW_DIR="$DIR/tmp-review"

GREEN_COLOR='\033[0;32m' # green color
RED_COLOR='\033[0;31m'   # red   color
RESET_COLOR='\033[0m'    # reset color

# inform about failed review
function reviewFailed {

	echo ''
	echo -e "${RED_COLOR}✗ Review failed... You can not commit!${RESET_COLOR}"
	echo ''

	exit 1;

}

# remove temporary directory
function removeTmpReviewDir {

	rm -rf "$TMP_REVIEW_DIR"

}

# check if temporary directory exists and prompt to remove it if it does
if [ -d "$TMP_REVIEW_DIR" ]; then

	echo ''
	echo "The temporary directory $TMP_REVIEW_DIR already exists!"
	echo "Run a following command to remove the directory:"
	echo ''
	echo "rm -rf $TMP_REVIEW_DIR"
	echo ''

	exit 1;

fi

# create temporary directory to make the review there
# making review in a separate directory avoids changing files in the working directory
# see http://stackoverflow.com/questions/20479794/how-do-i-properly-git-stash-pop-in-pre-commit-hooks-to-get-a-clean-working-tree
mkdir -p "$TMP_REVIEW_DIR"

# check if temporary directory was created
if [[ ! -d "$TMP_REVIEW_DIR" ]]; then

  echo ''
  echo -e "${RED_COLOR}Could not create temporary directory!${RESET_COLOR}"
  echo ''

  exit 1

fi

# register the reviewFailed function to be called on the ERR signal
trap reviewFailed ERR

# register the removeTmpReviewDir function to be called on exit
trap removeTmpReviewDir EXIT

# checkout all files from the index to the temporary directory
git checkout-index --prefix="$TMP_REVIEW_DIR/" -a

cd "$TMP_REVIEW_DIR"

echo ''
echo "The review is running, please wait..."
echo ''

# make the review (run jobs in parallel); ignore standard output
make review

echo ''
echo -e "${GREEN_COLOR}✓ All done! You are fine to commit! :)${RESET_COLOR}"
echo ''
